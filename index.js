const fs = require('fs');

const { Client } = require('pg');

const file = require('./lib/file');
const parser = require('./lib/parser');
const db = require('./lib/db');

var args = [];
var flags = [];

var cleanup = [];

process.argv.slice(2).forEach((arg) => {
	if (arg === '-') {
		flags.push('stdin');
	} else if (arg.substr(0,1) === '-') {
		flags.push(arg.replace(/^-+/, ''));
	} else {
		args.push(arg);
	}
});

async function getInputStream(args, flags) {
	if (flags.includes('stdin')) {
		if (args.length !== 0) {
			throw new Error(`Extra Args (${args.join(' ')})`);
		}
		console.log("Reading from stdin");
		return process.stdin;
	}

	if (args.length !== 1) {
		throw new Error('Requires a filename');
	}

	var streamName = args[0];
	if (streamName.startsWith('http')) {
		console.log("Copy from remote file", streamName);
		let {filename, cleanupCallback} = await file.downloadToFile(streamName);
		streamName = filename;
		cleanup.push(cleanupCallback);
	}
	console.log("Reading from local file", streamName);
	return fs.createReadStream(streamName);
}
		
const client = new Client();
client.connect();
cleanup.push(client.end.bind(client));

async function insertAll(gen) {
	for (;;) {
		let {value, done} = gen.next();
		if (done) {
			break;
		}
		try {
			var order = await value;
			await db.insertOrder(client, order);
		} catch (e) {
			if (order && order.orderId) {
				console.error("Error on order:", order.orderId, e.message);
			} else {
				console.error("Error on line:", e.message);
			}
		}
	}
}

getInputStream(args, flags)
.then(parser.lineGenerator)
.then(parser.csvToObjects)
.then(parser.objectsToOrders)
.then(insertAll)
.catch((e) => {
	console.error("Global:", e);
})
.then(() => {
	return Promise.all(cleanup.map((f) => f()));
});









