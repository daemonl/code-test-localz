
CREATE TABLE customer (
	id varchar(40) PRIMARY KEY,
	firstName text,
	lastName text
);

CREATE TABLE customer_order (
	id varchar(40) PRIMARY KEY,
	customer varchar(40) REFERENCES customer (id) NOT NULL,
	item text NOT NULL,
	quantity integer NOT NULL
);

INSERT INTO customer
	(id, firstName, lastName) 
VALUES
	('customer-321', 'Larry', 'Lunchbox');
