
const constraintErrors = {
	"customer_order_pkey": "Duplicate orderId",
	"customer_order_customer_fkey": "customerId not found",
};

async function insertOrder(client, order) {
	const qText = 'INSERT INTO customer_order (id, customer, item, quantity) VALUES ($1, $2, $3, $4)';
	const qParam = [order.orderId, order.customerId, order.item, order.quantity];
	try {
		await client.query(qText, qParam);
	} catch(e) {
		if (e.hasOwnProperty('constraint')) {
			let msg = constraintErrors[e.constraint];
			if (msg) {
				throw new Error(msg);
			}
		}
		throw e;
	}
}

async function getOrder(client, orderId) {
	const qText = 'SELECT id, customer, item, quantity FROM customer_order WHERE id = $1';
	const qParam = [orderId];
	const res = await client.query(qText, qParam);
	if (res.rows.length !== 1) {
		throw new Error(`No Order Matched ${orderId}`);
	}
	const row = res.rows[0];
	return {
		orderId: row.id,	
		customerId: row.customer,
		item: row.item,
		quantity: row.quantity,
	};
}

module.exports = {
	insertOrder,
	getOrder,
};
