const http = require('http');
const https = require('https');
const fs = require('fs');
const url = require('url');
const tmp = require('tmp');

/**
 * @typedef {Object} TemporaryFile
 * @property {string} filename
 * @property {?} fd
 * @property {Function} cleanupCallback
 */

/**
 * creates a temporary file, returning the filename and cleanupCallback to
 * delete the file
 * @promise <TemporaryFile>
 */
function newTempFile() {
	return new Promise((cbOK, cbErr) => {
		tmp.file((err, filename, fd, cleanupCallback) => {
			if (err) { return cbErr(err); }
			else { return cbOK({ filename, fd, cleanupCallback }); }
		});
	});
}

/** writes a stream to a file */
function streamToFile(stream) {
	return newTempFile()
	.then(({ filename, cleanupCallback }) => {
		var file = fs.createWriteStream(filename);
		return new Promise((cbOK, cbErr) => {
			const s = stream.pipe(file);
			s.on('finish', () => cbOK({filename, cleanupCallback}));
			s.on('error', cbErr);
		});
	});
}

/** downloads a url to a stream */
function downloadToStream(uri) {
	let { protocol } = url.parse(uri);
	let httpLib = null;
	if (protocol === "http:") {
		httpLib = http;
	} else if (protocol === "https:") {
		httpLib = https;
	} else {
		throw new Error(`Bad URL protocol: ${protocol}`);
	}

	return new Promise((cbOK, cbErr) => {
		let req = httpLib.get(uri, (res) => {
			const { statusCode } = res;
			if (statusCode !== 200) {
				cbErr(new Error(`Status ${statusCode}`));
			}
			cbOK(res);
		});

		req.on('error', cbErr);
	});
}

/** downloads a URL into a temporary file */
function downloadToFile(uri) {
	return downloadToStream(uri)
		.then(streamToFile);
}

module.exports = {
	streamToFile,
	downloadToStream,
	downloadToFile,
};

