const lineReader = require('line-reader');
const csvrow = require('csvrow');
const util = require('util');

class RowError extends Error {
	constructor(line, reason) {
		super(reason);
	}
}

/**
 * Generates lines from a stream.
 * @param {Stream}
 * @return {Iterable.<String>}
 */
async function lineGenerator(stream) {

	let reader = await util.promisify(lineReader.open)(stream);

	return function* () {
		while (true) {
			if (!reader.hasNextLine()) {
				return;
			}
			yield new Promise((cbOK, cbErr) => {
				if (!reader.hasNextLine()) {
					return cbErr();
				}
				reader.nextLine((err, line) => {
					if (err) { cbErr(err); }
					else { cbOK(line); }
				});
			});
		}
	}();

}

/**
 * Callback for map
 * @callback mapCallback
 * @param {*}
 * @return {*}
 */

/**
 * Returns a generator which runs the conversion on the input generator. Like
 * Array.prototype.map for Iterators Assumes the input will generate promises.
 * @param {Iterable.<*>} input
 * @param {mapCallback} callback
 */
function* generateMap(input, callback) {
	for (;;) {
		let next = input.next();
		if (next.done) {
			break;
		}
		yield next.value.then(callback);
	}
}

/**
 * Generates csv Objects for a lineGenerator.  Must have a header row, then the
 * keys in the header row are used to construct each object
 * @param {Iterable.<String>} lineGenerator
 * @promise {Iterable.<Object>}
 */
async function csvToObjects(lineGenerator) {
	let {value, done} = lineGenerator.next();
	if (done) {
		throw new Error("No Header Row");
	}

	let header = await value.then(csvrow.parse);

	return generateMap(lineGenerator,
		(lineRaw) => {
			let fields = csvrow.parse(lineRaw);
			if (fields.length !== header.length) {
				throw new RowError(fields, "Field Length");
			}
			let obj = {};
			header.forEach((key, i) => {
				obj[key] = fields[i];
			});
			return obj;
		}
	);
}

/**
 * Converts generic CSV objects to orders.
 * @param {Iterable<.Object>} objectGenerator
 * @return {Iterable<.Object>}
 */
function objectsToOrders(objectGenerator) {
	return generateMap(objectGenerator, parseObject);
}

function parseObject(raw) {
	return {
		orderId: raw.orderId,
		customerId: raw.customerId,
		item: raw.item,
		quantity: parseInt(raw.quantity),
	};
}

module.exports = {
	lineGenerator: lineGenerator,
	csvToObjects: csvToObjects,
	objectsToOrders: objectsToOrders,
};

