CSV Order Parser
================

Prepared in response to a coding test.

`npm test` runs mocha tests.
`node index.js {url}` downloads from a remote URL.

Environment variables control Postgres connection parameters.

```
PGHOST='localhost'
PGUSER=process.env.USER
PGDATABASE=process.env.USER
PGPASSWORD=null
PGPORT=5432
```

To run a postgres server locally with the data structure initialized, run
`docker-compose up` in the sql folder. This assumes you do not already have a
development server running, and so binds to host port 5432.

For convenience, the required connection parameters are exported in vars.sh,
run `source ./sql/vars.sh` to use them.

Coding Test
-----------

### The Challenge

Write a simple batch job that retrieves a CSV file from a URL, which imports
orders into a database. Assume there is an existing collection/table of
customers and orders. Ensure that the customerId exists in the database before
importing the order, otherwise skip the import for the order.


Implementation Notes
--------------------

This code should be 'production ready' with error handling, and should be 'fairly scalable'.

In a real world scenario, this script would not talk directly to the database,
but would instead call the microservice which owns orders, or more likely,
publish orders to a queue to be processed later.

### Download

The script must download from a remote URL. Rather than streaming directly from
an HTTP response body into the parser, the file is downloaded to a temporary
local file first.

This is a trade-off which increases the overall latency (download entire file,
then begin parsing) and requires enough local disk space for the file, the
benefit is that network issues with the remote server are isolated to a single
step when the script begins, and the TCP session between the server and client
aren't held open while the database operations run.

The script is also able to read directly from a local file, or from stdin,
allowing any number of other combinations to work using bash pipelining.

```
curl http://server/orders.csv | node index.js -
```

### Parse

The file is read line-by-line, and processing of the next line is blocked until
the current line is complete. This prevents a memory run-away. The script could
theoretically handle an infinite list of orders with finite memory (when using
stdin piping).

With extra time, it should be possible to run a fixed number of parallel workers
which consume the file generator and trigger database uploads in parallel, but
still with finite memory.

### Database

Contextual validation is handled by database constraints:

- Uniquness of order IDs
- Checking that the customer ID is valid

This is especially important as multiple services would be accessing the
database, but also gives a clear guarantee which can be relied upon.

'Normal' postgres errors are re-worded to present to the user.


### Testing

The tests currently rely on the remote server https://www.example.com. It could
/ should create a local test server instead.

The Postgres 'unit tests' connect to an actual database, so the SQL statements
are actually tested. This is not a strict unit-test, and may require tweaking
to mocha timeouts to be reliable. I think the trade-off is worth it for the
confidence this test gives

