const expect = require('chai').expect;
const fs = require('fs');
const { testStream } = require('./util');

const file = require('../lib/file');

describe("StreamToFile", function() {
	it("Should write to a file", function() {
		const stream = testStream({});
		return file.streamToFile(stream)
		.then(({filename, cleanupCallback}) => {
			let data = fs.readFileSync(filename);
			let lines = String(data).split("\n");
			expect(lines).to.have.length(12);
			return cleanupCallback();
		});
	});
});

describe("DownloadToFile", function() {
	it("Should download to a file", function() {
		return file.downloadToFile("https://www.example.com")
		.then(({filename, cleanupCallback}) => {
			let data = fs.readFileSync(filename);
			expect(String(data)).to.contain("Example Domain");
			return cleanupCallback();
		});
	});

	it("Should throw connection errors", function() {
		return file.downloadToFile("https://127.0.0.1:1")
		.then(() => {
			throw new Error("Expected Error");
		}, (err) => {
			expect(err.message).to.contain("ECONNREFUSED");
		});
	});
	
	it("Should throw status errors", function() {
		return file.downloadToFile("https://www.example.com/404.html")
		.then(() => {
			throw new Error("Expected Error");
		}, (err) => {
			expect(err.message).to.contain("404");
		});
	});
});
