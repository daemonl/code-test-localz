
const streamify = require('stream-generators');

let testStream = (options) => {
	const count = options.count || 10;
	const header = options.header || 'orderId,customerId,item,quantity';
	const rows = options.rows || ((i) => `sample-${i},customer-321,Flowers,2`);
	let i = 0;
	return streamify(function* () {
		yield header + '\n';
		while (count < 0 || i < count) {
			yield rows(i) + '\n';
			i++;
		}
	});
};

async function captureAll(gen) {
	let out = [];
	for (;;) {
		let {value, done} = gen.next();
		if (done) {
			break;
		}
		var line = await value;
		out.push(line);
	}
	return out;
}

module.exports = {
	captureAll,
	testStream,
};
