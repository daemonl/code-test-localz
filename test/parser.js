const expect = require('chai').expect;
const { testStream, captureAll } = require('./util');

const parser = require('../lib/parser');




describe("LineGenerator", function() {
	it("Should generate lines", function() {
		return parser.lineGenerator(
			testStream({})
		)
		.then(captureAll)
		.then((lines) => {
			expect(lines).to.have.length(11);
		});
	});
});

describe("CSVToObjects", function() {
	it("Should chain to objects", function() {
		return parser.lineGenerator(
			testStream({})
		)
		.then(parser.csvToObjects)
		.then(captureAll)
		.then((lines) => {
			expect(lines).to.have.length(10);
			let line = lines[0];
			expect(line.orderId).to.equal('sample-0');
			expect(line.customerId).to.equal('customer-321');
			expect(line.item).to.equal('Flowers');
			expect(line.quantity).to.equal('2');
		});
	});
});

describe("CSVToOrders", function() {
	it("Should chain to orders", function() {
		return parser.lineGenerator(
			testStream({})
		)
		.then(parser.csvToObjects)
		.then(parser.objectsToOrders)
		.then(captureAll)
		.then((lines) => {
			expect(lines).to.have.length(10);
			let line = lines[0];
			expect(line.orderId).to.equal('sample-0');
			expect(line.customerId).to.equal('customer-321');
			expect(line.item).to.equal('Flowers');
			expect(line.quantity).to.equal(2);
		});
	});

});
