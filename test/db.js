const expect = require('chai').expect;
const { Client } = require('pg');
const uuidv1 = require('uuid/v1');

const db = require('../lib/db');


describe("Database", function() {
	let client;
	before(async function() {
		client = new Client();
		client.connect();
	});
	
	after(function() {
		return client.end();
	});

	it("Should fail with no data", function() {
		return db.insertOrder(client, {}).
		then(() => {
			expect.fail("Error", "Success");
		}, (e) => {
			expect(e.message).to.contain("not-null");
		});
	});

	it("Should insert an order", async function() {
		let orderId = uuidv1();
		await db.insertOrder(client, {
			orderId: orderId,
			customerId: "customer-321",
			item: 'Flowers',
			quantity: 3,
		});

		let order = await db.getOrder(client, orderId); 
		expect(order.orderId).to.equal(orderId);
		expect(order.quantity).to.equal(3); // not string
	});
	
	it("Should reject duplicate order", async function() {
		let orderId = uuidv1();
		await db.insertOrder(client, {
			orderId: orderId,
			customerId: "customer-321",
			item: 'Flowers',
			quantity: 3,
		});

		let order = await db.getOrder(client, orderId); 
		expect(order.orderId).to.equal(orderId);
		expect(order.quantity).to.equal(3); // not string

		return db.insertOrder(client, {
			orderId: orderId,
			customerId: "customer-321",
			item: 'Flowers',
			quantity: 3,
		})
		.then(() => {
			expect.fail("Error", "Success");
		}, (e) => {
			expect(e.message).to.contain("Duplicate orderId");
		});
	});
	
	it("Should reject bad customer", async function() {
		let orderId = uuidv1();
		return db.insertOrder(client, {
			orderId: orderId,
			customerId: "nemo",
			item: 'Flowers',
			quantity: 3,
		})
		.then(() => {
			expect.fail("Error", "Success");
		}, (e) => {
			expect(e.message).to.contain("customerId not found");
		});
	});
});
